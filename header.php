<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package materialwp
 */
?><!DOCTYPE html>
<html <?php //language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href = "<?php bloginfo('stylesheet_url');?>" rel = "stylesheet">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'materialwp' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="home-hero" style="background-color: #2196F3;margin-top: -25px;color: #fff;padding: 60px 0;margin-bottom: -10px;">
        			<div class="container">
        				<h1>GMO ベトナムラボ</h1>
        			</div>
        </div>

		<nav class="navbar navbar-inverse" role="navigation">

		  <div class="container">

			<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<div class="icon-preview"><i class="mdi-action-home"></i></div>
			</a>

			<div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">
            				 <?php
            		            wp_nav_menu( array(
            		                'menu'              => 'primary',
            		                'theme_location'    => 'primary',
            		                'depth'             => 2,
            		                'container'         => false,
            		                'menu_class'        => 'nav navbar-nav',
            		                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
            		                'walker'            => new wp_bootstrap_navwalker())
            		            );
            	        	?>
			<form class="navbar-form navbar-right" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
				<input name="s" id="s" type="text" class="form-control" placeholder="<?php _e('Search','responsive'); ?>">
			</form>

        	</div><!-- /.container -->
		</nav><!-- .navbar .navbar-default -->

		<!--
		<p class="bs-component">
			<a href="javascript:void(0)" class="btn btn-default">Default</a>
			<a href="javascript:void(0)" class="btn btn-primary">Primary</a>
			<a href="javascript:void(0)" class="btn btn-success">Success</a>
			<a href="javascript:void(0)" class="btn btn-info">Info</a>
			<a href="javascript:void(0)" class="btn btn-warning">Warning</a>
			<a href="javascript:void(0)" class="btn btn-danger">Danger</a>
			<a href="javascript:void(0)" class="btn btn-link">Link</a>
		</p>
		-->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
